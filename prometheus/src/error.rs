use serde_derive::Serialize;
use snafu::Snafu;
use std::string::FromUtf8Error;
use std::sync::Arc;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum PromMetricErr {
    #[snafu(display("Encoding Metrics: {}", message))]
    Encoding { message: String },
    #[snafu(display("Converting buffer to string: {}", source))]
    ConvertBuffer {
        #[snafu(source(from(std::string::FromUtf8Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::string::FromUtf8Error>,
    },
    #[snafu(display("Issue stringifying metrics: {}", source))]
    StringConversion {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<FromUtf8Error>,
    },
    #[snafu(display("Error in prometheus lib: {}", source))]
    PrometheusErr {
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<prometheus::Error>,
    },
}

impl From<FromUtf8Error> for PromMetricErr {
    fn from(e: FromUtf8Error) -> Self {
        PromMetricErr::StringConversion {
            source: Arc::new(e),
        }
    }
}

impl From<prometheus::Error> for PromMetricErr {
    fn from(e: prometheus::Error) -> Self {
        PromMetricErr::PrometheusErr {
            source: Arc::new(e),
        }
    }
}
