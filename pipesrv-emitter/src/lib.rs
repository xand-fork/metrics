#![forbid(unsafe_code)]

pub mod error;
mod logging_events;

use error::{Error, Result};
use logging_events::LoggingEvent;
use std::{
    fs,
    io::Write,
    os::unix::net::{UnixListener, UnixStream},
    path::Path,
    thread,
    time::Duration,
};
use tpfs_logger_port::error;
use xand_metrics::Metrics;

pub const SOCKET_PATH: &str = "/var/pipesrv/pipe";

pub const EMIT_METRICS_ENV_VAR: &str = "EMIT_METRICS";

/// Spin up a thread that will send the metrics to socket streams on a regular `cadence` in seconds using
/// provided implementation of xand_metrics::Metrics. Will do nothing if fails
pub fn spin_up_emitter(cadence: u64, metrics: Box<impl Metrics + Send + Sync + 'static>) {
    if let Some(sender) = new_sender_for_socket_streams(SOCKET_PATH) {
        thread::spawn(move || loop {
            if emitting_metrics() {
                if let Ok(output) = metrics.produce() {
                    match bincode::serialize(&output) {
                        Ok(o) => sender.send(o).unwrap_or_else(|e| {
                            error!(LoggingEvent::FailedToSendMetricOverChannel(e.to_string()));
                        }),
                        Err(e) => error!(LoggingEvent::FailedToSerializeMetric(e.to_string())),
                    }
                };
            }
            thread::sleep(Duration::from_secs(cadence))
        });
    }
}

/// Creates socket and sender/receiver pair. Receiver is passed to socket streams in handers and
/// the sender is returned for accessing those handlers.
fn new_sender_for_socket_streams(path: &str) -> Option<crossbeam_channel::Sender<Vec<u8>>> {
    let (sender, receiver) = crossbeam_channel::unbounded();
    let handler = recv_and_write_to_stream_handler_factory(receiver);
    match new_socket_with_handler(path, handler) {
        Ok(_) => Some(sender),
        Err(e) => {
            error!(LoggingEvent::FailedToCreateSocketForHandler(e.to_string()));
            None
        }
    }
}

/// Opens a socket listener that will assign a handler to any streams it sees connect
fn new_socket_with_handler(
    path: &str,
    handler: (impl Fn(UnixStream) + Send + Sync + 'static),
) -> Result<()> {
    let path = Path::new(path).to_path_buf();
    if path.exists() {
        fs::remove_file(&path).map_err(|e| Error::UnderlyingError {
            str_source: format!("Couldn't delete path at {:?}, {:?}", &path, e),
        })?;
    }
    let listener = UnixListener::bind(path.clone()).map_err(|e| Error::UnderlyingError {
        str_source: format!("Could not bind to socket at {:?}, {:?}", &path, e),
    })?;
    thread::spawn(move || {
        for stream in listener.incoming() {
            match stream {
                Ok(s) => handler(s),
                Err(e) => {
                    error!(LoggingEvent::ListenerExitingWithError(e.to_string()));
                    break;
                }
            };
        }
    });
    Ok(())
}

/// Creates a handler that waits for a message from a channel and sends the message over the socket
fn recv_and_write_to_stream_handler_factory(
    receiver: crossbeam_channel::Receiver<Vec<u8>>,
) -> (impl Fn(UnixStream) + Send + Sync + 'static) {
    move |mut stream: UnixStream| {
        let r = receiver.clone();
        thread::spawn(move || loop {
            match r.recv() {
                Ok(msg) => stream.write_all(&msg).expect("Can't write to stream"),
                Err(e) => {
                    error!(LoggingEvent::ReceiverThreadExitingWithError(e.to_string()));
                    break;
                }
            }
        });
    }
}

/// Returns `true` if env_var matching the const `EMIT_METRICS_ENV_VAR` is set to anything but 'false'
fn emitting_metrics() -> bool {
    let val = std::env::var(EMIT_METRICS_ENV_VAR).unwrap_or_else(|_| "false".to_string());
    &val != "false"
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{os::unix::net::UnixStream, path::Path};
    use xand_metrics::MetricsSerializationContainer;

    #[test]
    fn test_new_sender_for_socket_streams() {
        let path_str = "/tmp/test_output_to_socket";
        let path = Path::new(path_str).to_path_buf();
        let sender = new_sender_for_socket_streams(path_str).unwrap();
        let stream = UnixStream::connect(path).unwrap();
        let output = vec![
            MetricsSerializationContainer {
                metrics: "bleep".to_string(),
            },
            MetricsSerializationContainer {
                metrics: "bloop".to_string(),
            },
            MetricsSerializationContainer {
                metrics: "blorp".to_string(),
            },
            MetricsSerializationContainer {
                metrics: "yort".to_string(),
            },
        ];
        for o in output.clone() {
            let msg = bincode::serialize(&o).unwrap();
            sender.send(msg).unwrap();
        }
        for o in output {
            let read_msg: MetricsSerializationContainer =
                bincode::deserialize_from(&stream).unwrap();
            assert_eq!(o, read_msg);
        }
    }

    #[test]
    fn test_emitting_metrics_true_for_true() {
        // "true" is true
        std::env::set_var(EMIT_METRICS_ENV_VAR, "true");
        assert!(emitting_metrics());

        // "false" is false
        std::env::set_var(EMIT_METRICS_ENV_VAR, "false");
        assert!(!emitting_metrics());

        // if it is set, it is true
        std::env::set_var(EMIT_METRICS_ENV_VAR, "not_true_or_false");
        assert!(emitting_metrics());

        // if it is unset, it is false
        std::env::remove_var(EMIT_METRICS_ENV_VAR);
        assert!(!emitting_metrics());
    }
}
