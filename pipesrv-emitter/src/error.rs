use serde_derive::Serialize;
use snafu::Snafu;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum Error {
    #[snafu(display("{}", str_source))]
    UnderlyingError { str_source: String },
}
