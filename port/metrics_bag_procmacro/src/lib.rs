extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DeriveInput, Fields, Type};

/// This macro implements [`MetricsBag`](../xand_metrics/trait.MetricsBag.html) for named structs.
///
/// The default implementation of `list_metrics()` requires each field to be one of the inner values
/// of the types defined in the enum [`xand_metrics::MetricDefs`](../xand_metrics/enum.MetricDefs.html). It takes each field and wraps it
/// in the corresponding `MetricDef` variant and adds it to a vector, and returns that vector.
///
/// The default implementation of `as_any()` simply returns `self`.
#[proc_macro_derive(MetricsBag)]
pub fn metrics_bag(input: TokenStream) -> TokenStream {
    let input: DeriveInput = syn::parse(input).unwrap();
    // Error out if we're not annotating a struct
    let data = match input.data {
        Data::Struct(d) => d,
        _ => panic!("MetricsBag can only be derived for named structs"),
    };
    let name = &input.ident;
    let list_metrics_method_contents = match &data.fields {
        Fields::Named(fields) => {
            let push_metric_defs_to_vec = fields.named.iter().map(|f| {
                let field_name = &f.ident.clone().unwrap();
                let f_type_name;
                if let Type::Path(p) = &f.ty {
                    f_type_name =
                        format!("{}", p.path.segments.first().unwrap().into_value().ident);
                } else {
                    panic!("Invalid metric def!")
                }
                let metric_def = match f_type_name.as_str() {
                    "CounterDef" => {
                        quote!(::xand_metrics::MetricDefs::Counter(&mut self.#field_name))
                    }
                    "GaugeDef" => quote!(::xand_metrics::MetricDefs::Gauge(&mut self.#field_name)),
                    "HistogramDef" => quote!(::xand_metrics::MetricDefs::Histogram(&mut self.#field_name)),
                    "DimensionedCtrDef" => {
                        quote!(::xand_metrics::MetricDefs::DimensionedCtr(&mut self.#field_name))
                    }
                    "DimensionedGaugeDef" => {
                        quote!(::xand_metrics::MetricDefs::DimensionedGauge(&mut self.#field_name))
                    }
                    "DimensionedHistoDef" => {
                        quote!(::xand_metrics::MetricDefs::DimensionedHistogram(&mut self.#field_name))
                    }
                    a => panic!("Invalid metric def: {}", a),
                };
                quote!(vec.push(#metric_def);)
            });
            quote! {
               let mut vec = Vec::new();
               #(#push_metric_defs_to_vec)*
               vec
            }
        }
        _ => panic!("MetricsBag can only be derived for named structs"),
    };
    let the_impl = quote! {
        impl ::xand_metrics::MetricsBag for #name {
            fn list_metrics(&mut self) -> Vec<::xand_metrics::MetricDefs> {
                #list_metrics_method_contents
            }
            fn as_any(&self) -> &dyn std::any::Any {
                self
            }
        }
    };
    TokenStream::from(the_impl)
}
