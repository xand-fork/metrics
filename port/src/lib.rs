//! # Using `xand-metrics` library
//!
//!
//! The `xand-metrics` library provides a port for defining domain specific metrics and adapting them to
//! your metrics backend.
//!
//! In order to add a metric to your instrumentation
//!
//! 1. Define your metric in your MetricsBag
//! 2. Initialize your metric bag with your Metric adaptor
//! 3. Add the MetricsBag fields' method calls to your code
//! 4. Have metrics emitted to std_out
//!
//! #### Define your metric in your MetricsBag
//!
//! A metric bag is the global set of metrics you intend to track. The types of each field must be some metric
//! definition (`Def`) as defined in the [`MetricDefs`](enum.MetricDefs.html) library.
//!
//! Use [`#[derive(MetricsBag)]`](../metrics_bag_procmacro/derive.MetricsBag.html) provided by the port crate
//! to implement the [`MetricsBag`](trait.MetricsBag.html) trait for you.
//!
//! #### Initialize your metric bag with your Metric adaptor
//!
//! Before using your metrics bag, you must have it initialized. Your Metric implementation will provide
//! an [`initialize()`](trait.Metrics.html#tymethod.initialize) api. This function will inject your metric dependency for each definition, as defined by
//! your backend metric adaptor.
//!
//! For Xand, the Metrics implementation is normally the Prometheus implementation.
//!
//! ####  Add the MetricsBag fields' method calls to your code
//!
//! Now you are free to use your metric bag as you please! Simply use the metrics you've defined, which are
//! the fields of the metrics bag type.
//!
//! #### Have metrics emitted to std_out
//!
//! In order to expose your metrics to std_out, you must set the env_var `EMIT_METRICS` to `true`
//! and either call [`Metrics::emit`](trait.Metrics.html#method.emit) or run [`spin_up_emitter`](fn.spin_up_emitter.html)
//! which will call `emit()` on a regular cadence.
//!
//! #### Example
//!
//! ```
//! use xand_metrics::*;
//! #[derive(MetricsBag)]
//! pub struct TestBag {
//!     pub test_ctr: CounterDef,
//!     pub test_gauge: GaugeDef,
//!     pub test_histo: HistogramDef,
//!     pub test_dim_ctr: DimensionedCtrDef,
//!     pub test_dim_gauge: DimensionedGaugeDef,
//!     pub test_dim_histo: DimensionedHistoDef,
//! }
//!
//! impl Default for TestBag {
//!     fn default() -> Self {
//!         Self {
//!             test_ctr: CounterDef::new("test_counter", "counter help!"),
//!             test_gauge: GaugeDef::new("test_gauge", "gauge help!"),
//!             test_histo: HistogramDef::new("test_histo", "histogram help!"),
//!             test_dim_ctr: DimensionedCtrDef::new(
//!                 vec!["a"],
//!                 MetricOpts::new(
//!                     "test_counter_vec".to_string(),
//!                     "I have dimensions!".to_string(),
//!                 ),
//!             ),
//!             test_dim_gauge: DimensionedGaugeDef::new(
//!                 vec!["a", "b"],
//!                 MetricOpts::new(
//!                     "test_gauge_vec".to_string(),
//!                     "I have dimensions!".to_string(),
//!                 ),
//!             ),
//!             test_dim_histo: DimensionedHistoDef::new(
//!                 vec!["a", "b", "c"],
//!                 MetricOpts::new(
//!                     "test_histo_vec".to_string(),
//!                     "I have dimensions!".to_string(),
//!                 ),
//!             ),
//!         }
//!     }
//! }
//! let mut my_bag = TestBag::default();
//! let my_metrics = FakeMetrics::default();
//! let _ = my_metrics.initialize(&mut my_bag);
//! my_bag.test_ctr.inc();
//! my_bag.test_dim_ctr.with_dims(vec!["1", "2", "3"])?.inc();
//! # Ok::<(), MetricError>(())
//! ```
//!
//! Keep in mind, in this example the `TestBag` "`tb`" is initialized in the same scope as it is used. In
//! many applications you will have to provide some way to make the metric bag accessible globally.
//!
//! An example would be if you are metering the methods on a trait, you can provide a decorator for that trait that wraps the
//! methods. That way the decorator is publicly available and internally uses a reference to a [`lazy_static`](../lazy_static/index.html)
//! of the `MetricsBag`.  See [`xand-banks::metrics`](https://gitlab.com/TransparentIncDevelopment/thermite/blob/develop/xand-banks/src/metrics.rs)
//! for an example.
//!
//! ### Metric types
//!
//! The port supports 3 metric options: counter, gauge, and histogram. Each has two flavors: plain and dimensioned.
//!
//! #### Definitions
//!
//! The plain metrics take two fields:
//!
//! `name` - The name of the metric.
//!
//! `help` - Additional details on the use of the metric.
//!
//! The dimensioned metrics take two different fields:
//!
//! `dimensions` - A vector of names for each dimension of the metric. These names will be mapped one-to-one
//! with values given in the instrumentation.
//!
//! For example, your metric might be a counter for transactions on the Xand network. The names for the dimensions
//! might be `["type", "status"]` where "type" can be transfer, creation_request, or redeem_request, etc, and the
//! "status" could be committed or invalid.
//!
//! `opts` - [`MetricOpts`](struct.MetricOpts.html) object with `name` and description `desc` fields.
//!
//! The "dimensioned" variants will require you to specify the dimensions using the `with_dims()` method. (See
//! the above example.)
//!
//! You will want to include a default instance definition as well, as the MetricsBag will generally.
//! only have a single instance.
//!
//!#### Choosing the right metric for you
//!
//! [`Counter`](trait.Counter.html)
//!
//! * For tracking the discrete count of some item or items
//! * Unsigned integer [u64]
//! * Can only increase or stay constant
//!
//! [`Gauge`](trait.Gauge.html)
//!
//! * For tracking non-discrete and/or fluctuating values
//! * Floating point [f64]
//! * Can increase or decrease
//!
//! [`Histogram`](trait.Histogram.html)
//!
//! * For tracking the distribution of values for a given subject
//! * Floating point [f64]
//! * Observes each occurrence and aggregates them into a larger metric
//!
//!

#![forbid(unsafe_code)]

mod error;
pub mod test_help;

pub use crate::error::MetricError;
// No way I could see to keep this private but usable in doc/integ tests
pub use crate::test_help::FakeMetrics;
use derive_more::Constructor;
pub use metrics_bag_procmacro::MetricsBag;
use serde_derive::{Deserialize, Serialize};
use std::{any::Any, fmt::Debug, ops::Deref};

pub type Result<T, E = MetricError> = std::result::Result<T, E>;

#[derive(Serialize, Deserialize, Constructor, Debug, Eq, PartialEq, Clone)]
pub struct MetricsSerializationContainer {
    pub metrics: String,
}

/// Adapters must implement this interface to provide metrics
pub trait Metrics {
    type ErrorT: Debug;

    /// Given a metrics bag, this metrics implementation takes those definitions and creates its
    /// concrete versions of the counters, and then registers them or does whatever setup is
    /// required.
    fn initialize<T: MetricsBag>(&self, metrics_bag: &mut T) -> Result<(), Self::ErrorT>;
    /// Called by ['spin_up_emitter()'] to produce metrics in a way we can serialize. The adapter
    /// defines the format of the metrics themselves.
    fn produce(&self) -> Result<MetricsSerializationContainer, Self::ErrorT>;
}

/// A metrics bag is a struct whose fields are all metrics definitions. Implementations are
/// generated by using [`#[derive(MetricsBag)]`](../metrics_bag_procmacro/derive.MetricsBag.html). Adapters are expected to call `list_metrics` inside
/// their [Metrics::initialize](trait.Metrics.html#tymethod.initialize) function.
pub trait MetricsBag: Send + Sync {
    fn list_metrics(&mut self) -> Vec<MetricDefs>;
    fn as_any(&self) -> &dyn Any;
}

/// All metric types are represented in this enum. Adapters must be able to create implementations
/// of each metric defition.
pub enum MetricDefs<'a> {
    Counter(&'a mut CounterDef),
    Gauge(&'a mut GaugeDef),
    Histogram(&'a mut HistogramDef),
    DimensionedCtr(&'a mut DimensionedCtrDef),
    DimensionedGauge(&'a mut DimensionedGaugeDef),
    DimensionedHistogram(&'a mut DimensionedHistoDef),
}

/// Information that is common accross all metric types
#[derive(Constructor)]
pub struct MetricOpts {
    /// The name of this metric
    pub name: String,
    /// Some text describing the purpose of the metric
    pub desc: String,
}

pub trait Metric {}
impl<T: Metric> Metric for Box<T> {}

pub struct MetricDef<M: Metric + ?Sized> {
    opts: MetricOpts,
    inner: Option<Box<M>>,
}
impl<M: Metric + ?Sized> MetricDef<M> {
    pub fn new<T: ToString>(name: T, help: T) -> Self {
        Self {
            opts: MetricOpts::new(name.to_string(), help.to_string()),
            inner: None,
        }
    }
    pub fn opts(&self) -> &MetricOpts {
        &self.opts
    }
    pub fn populate(&mut self, metric: Box<M>) {
        self.inner = Some(metric);
    }
}
impl<M: Metric + ?Sized> Deref for MetricDef<M> {
    type Target = M;

    fn deref(&self) -> &M {
        // The `expect` here is very much intentional. We want to crash if the user tries to
        // manipulate metrics in the bag without registering it first.
        self.inner
            .as_ref()
            .expect("Bag must be registered before recording metrics")
            .as_ref()
    }
}

pub type CounterDef = MetricDef<dyn Counter>;
pub type GaugeDef = MetricDef<dyn Gauge>;
pub type HistogramDef = MetricDef<dyn Histogram>;

/// A counter is a metric composed of a monotonically increasing value
pub trait Counter: Metric + Send + Sync {
    fn inc(&self);
    fn add(&self, delta: u64);
    fn get(&self) -> u64;
}

/// A gauge is a metric that may go up and down arbitrarily
pub trait Gauge: Metric + Send + Sync {
    fn inc(&self);
    fn add(&self, delta: f64);
    fn get(&self) -> f64;
    fn set(&self, val: f64);
    fn dec(&self);
    fn sub(&self, delta: f64);
}

/// A histogram is a metric which records samples of some value, and provides an aggregated,
/// bucketed, representation of the observations of that value over some time period.
pub trait Histogram: Metric + Send + Sync {
    fn observe(&self, v: f64);
}

/// Internally used to store common data for all "dimensioned" metrics
pub struct DimensionDef<M: Metric + ?Sized> {
    /// A metric measures something, but it may measure that thing across different "dimensions".
    /// A dimension segregates the metric. An example would be transaction volume, segregated by
    /// transaction type. In this case you would want a "type" dimension whose domain is "transfer",
    /// "create", "redeem", etc.
    dims: Vec<&'static str>,
    opts: MetricOpts,
    inner: Option<Box<dyn Dimensioned<M>>>,
}
impl<M: Metric + ?Sized> DimensionDef<M> {
    pub fn new(dimensions: Vec<&'static str>, opts: MetricOpts) -> Self {
        Self {
            dims: dimensions,
            opts,
            inner: None,
        }
    }

    /// Adapters must call this during their [Metrics::initialize](trait.Metrics.html#tymethod.initialize) implementation, passing the
    /// underlying implementation to "fill in" the metric definition.
    pub fn populate(&mut self, metric: Box<dyn Dimensioned<M>>) {
        self.inner = Some(metric)
    }

    pub fn opts(&self) -> &MetricOpts {
        &self.opts
    }

    /// Returns the dimensions of this metric
    pub fn dimensions(&self) -> &[&str] {
        self.dims.as_ref()
    }
}
impl<M: Metric + ?Sized> Deref for DimensionDef<M> {
    type Target = dyn Dimensioned<M>;

    fn deref(&self) -> &Self::Target {
        self.inner
            .as_ref()
            .expect("Bag must be initialized before recording metrics")
            .as_ref()
    }
}

/// "Dimensioned" metrics can be recorded across multiple dimensions. See [DimensionDef](struct.DimensionDef.html) for
/// more information.
pub trait Dimensioned<M: Metric + ?Sized>: Metric + Send + Sync {
    /// Fetch a specific metric from this dimensioned metric by providing values for all dimensions.
    /// For example if this metric has two dimensions "color" and "shape", you might pass
    /// `vec!["red", "triangle"]`. Note that the order of the passed in values is assumed to
    /// be the same as the order they were defined in.
    fn with_dims(&self, dim_vals: Vec<&str>) -> Result<Box<M>>;
}

/// A counter with one or more dimensions
pub type DimensionedCtrDef = DimensionDef<dyn Counter>;
/// A gauge with one or more dimensions
pub type DimensionedGaugeDef = DimensionDef<dyn Gauge>;
/// A histogram with one or more dimensions
pub type DimensionedHistoDef = DimensionDef<dyn Histogram>;
